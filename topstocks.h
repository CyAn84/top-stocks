#pragma once

#include <map>
#include <list>


//! \brief The StockRecord structure stores stock's record data.
typedef struct S {
    int id;
    double basePrice;
    double currentPrice;
    double change;
} StockRecord;


//! \brief The Topstocks class is for calculating the most gained and the most lose stocks.
class Topstocks
{
public:
    Topstocks();
    virtual ~Topstocks();

    void OnQuote(int stock_id,double price);

private:
    std::map<int, double> m_basePrices;
    std::list<StockRecord> m_topGainers;
    std::list<StockRecord> m_topLosers;

    void processRecord(const StockRecord &r);
    void toplistChanged(const std::list<StockRecord> *tl);
    void topGainersChanged();
    void topLosersChanged();

    void printRecords(const std::list<StockRecord> &list);
};
