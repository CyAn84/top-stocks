#include "topstocks.h"

#include <iterator>
#include <cmath>
#include <cstdio>


constexpr const size_t ITEMS_TO_SHOW = 10; // Toplist to show to user
constexpr const size_t ITEMS_TO_STORE = ITEMS_TO_SHOW * 10; // Whole toplist to have some cache of records


Topstocks::Topstocks()
{

}

Topstocks::~Topstocks()
{

}

void Topstocks::OnQuote(int stock_id, double price)
{
    auto it = m_basePrices.find(stock_id);
    if(it != m_basePrices.end()) {
        StockRecord tmp;
        tmp.id = stock_id;
        tmp.basePrice = m_basePrices[stock_id];
        tmp.currentPrice = price;
        tmp.change = (tmp.currentPrice - tmp.basePrice) * 100 / tmp.basePrice; // 100 to get percentage

        processRecord(tmp);

    } else { // stock id isn't in our list
        m_basePrices[stock_id] = price;
    }
}

void Topstocks::processRecord(const StockRecord &r)
{
    std::list<StockRecord> *toplist = nullptr;
    bool needNotify = false;

    if (r.change == 0) { // Weird but better to check
        return;
    } else if (r.change > 0) {
        toplist = &m_topGainers; // It is made to get rid of two almost same branches
    } else {
        toplist = &m_topLosers;
    }

    if (toplist->empty()) {
        toplist->push_back(std::move(r));
        needNotify = true;
    } else {
        auto it = toplist->begin();
        size_t cnt = 0; // Counter for limit list and notifications of top10 change
        bool inserted = false;

        while ((it != toplist->end()) && (cnt < ITEMS_TO_STORE)) {
            if (r.id == it->id) {
                if (r.change == it->change) {
                    return;
                } else {
                    it = toplist->erase(it);
                    (cnt < ITEMS_TO_SHOW) ? needNotify = true : needNotify;
                }
            }
            if (!inserted && std::abs(r.change) > std::abs(it->change)) {
                toplist->insert(it, std::move(r));
                inserted = true;
                (cnt < ITEMS_TO_SHOW) ? needNotify = true : needNotify;
            }
            it++;
            cnt++;
        }
        if (it == toplist->end() && cnt < ITEMS_TO_STORE && !inserted) {
            toplist->insert(it, std::move(r));
            inserted = true;
            (cnt < ITEMS_TO_SHOW) ? needNotify = true : needNotify;
        }
    }
    if (needNotify)
        toplistChanged(toplist);
}

void Topstocks::toplistChanged(const std::list<StockRecord> *tl)
{
    if (tl == &m_topGainers) {
        topGainersChanged();
    } else if (tl == &m_topLosers) {
        topLosersChanged();
    }
}

void Topstocks::topGainersChanged()
{
    printRecords(m_topGainers);
}

void Topstocks::topLosersChanged()
{
    printRecords(m_topLosers);
}

void Topstocks::printRecords(const std::list<StockRecord> &list)
{
    std::printf("Stock id\tFirst price\tLast price\tChange\n");
    auto it = list.cbegin();
    const size_t lim = (list.size() < ITEMS_TO_SHOW) ? list.size() : ITEMS_TO_SHOW;
    for (size_t i = 0; i < lim; i++) {
        std::printf("%d\t\t%.2f\t\t%.2f\t\t%.2f\n", it->id, it->basePrice, it->currentPrice, it->change);
        it++;
    }
}

